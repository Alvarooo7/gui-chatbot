
import time
import tkinter
from tkinter import *
    
#------------------------------------------------------------------------------------------

"""  colors """

c1 = '#263238'
c2 = '#faa21f'
c3 = '#1e282d'
c6 = '#577e75'

c4 = '#faa21f'
c5 = '#577e75'

c7 = '#1e282d'
c8 = '#faa21f'

def info () :

    global myname
    myname = "USER"
                                                                        
    global chatbot                           
    chatbot = "ROBOY"
    
#------------------------------------------------------------------------------------------



def topic_1():
    global no_topic
    no_topic = 1
    
    global top
    top = 'd1_technology.txt'

    global a
    a = open( top , 'r')

    global doc
    doc = a.readlines()

    frame_chat.pack()    

    topic = 'CHATBOT'
    label_topic.config(text = topic)

    refresh_screen()

    
def feed_answer () :

    global window
    window=Tk()

    frame_root = Frame(window , bg = c1)
    frame_root.pack()
                                                                                        

#----------------------------------------------------------------------------------------------

def refresh_screen () :

    for widget in frame_chats.winfo_children():
        widget.destroy()

    label_space = Label (frame_chats , bg = c1 ,  text = '')
    label_space.pack()

#------------------------------------------------------------------------------------------

def submit() :

    global chat_raw
    chat_raw = entry.get('1.0' , 'end-1c')
                        
    entry.delete('1.0' , END)
    
    chat = chat_raw.lower()
    chat = chat.replace(' ','')

    global label_request
    label_request = Label(frame_chats ,text=chat_raw , bg = c4 , fg= c7  , justify = LEFT , wraplength = 300, font = 'Verdana 10 bold')
    
    label_request.pack(anchor = 'w')   
    
    global answer

    if chat == "Hola" or chat == "hi" or chat == "hola" or chat == "que tal" or chat == 'que tal?' :
          answer = "Hola"

    elif chat == 'bye' or chat == 'goodbye' or chat == 'chau' or chat == 'exit' or chat == 'close' or chat == 'end' :
          answer = 'Bye'

    else:
        i = 0
        j = 0
        for lines in doc:
             stats = lines [:-1]
             stats = stats.lower()
             stat = stats.replace(' ','')
             i += 1
             if stat == chat :
                    answer = doc[i]
                    break
             else:
                 j += 1
                 
        if i == j :
              answer = "No te llego a entender.........escríbelo otra vez ! "

    get_response()
        
def get_response() :

    global label_response
    label_response = Label(frame_chats ,text= answer ,bg= c5 , fg = c8 , justify = LEFT , wraplength = 300, font = 'Verdana 10 bold')

    label_response.pack(anchor = 'e')

    if answer ==  'Bye':
        root.destroy()


#------------------------------------------------------------------------------------------------------------------



#-----------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------
"""
Creando ventana

"""

root = Tk ()  

#----------------------------------------------------------------------------------------------------

"""  images resources """

exitt = PhotoImage(file = 'exit.png')

submit_img = PhotoImage(file = 'image_8.png')


#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

"""         CHAT MAIN   """

frame_chat = Frame (root , bg = c1 , height = '670' , width = '550')
frame_chat.pack_propagate(0)
frame_chat.pack() 

frame_top = Frame( frame_chat , bg = c3 , height = '100' , width = '550')
frame_top.pack()

label_topic = Label ( frame_top , bg = c3 , fg = 'white' , font = 'Verdana 20 bold ')
label_topic.pack(pady = '40')

frame_spacer = Frame( frame_top , bg = c2 , height = "10" , width = "550" )
frame_spacer.pack()

bottom_frame = Frame (frame_chat , bg = c2 , height = '100' , width = '550')
bottom_frame.pack_propagate(0)
bottom_frame.pack(side = BOTTOM)

button = Button (bottom_frame , image = submit_img , relief = "flat", font = 'Vardana 10 bold' , bg = c3 , command = submit )
button.place(x = 410 , y = 27)
                                   
entry = Text (bottom_frame , bg = c3 , fg = c6 , height = '5'  , width ='45' , font  ='Verdana 10')
entry.bind ('<Return>' , submit)
entry.place(x = 30, y = 10)

frame_chats = Frame (frame_chat , bg = c1 , height = '450' , width = '500' )
frame_chats.pack_propagate (0)
frame_chats.pack()

label_space = Label(frame_chats , bg = c1).pack()


button_front = Button (frame_chat , image = exitt , relief = "flat" , bg = c3 , command = root.destroy ).place(x=440 , y = 10)

topic_1()
#-----------------------------------------------------------------------------------------------------------

root.mainloop ()
